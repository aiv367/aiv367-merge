import { defineConfig } from 'vite'
import dts from 'vite-plugin-dts'
import replace from '@rollup/plugin-replace';
import pkg from './package.json';
const d = new Date();

export default defineConfig({
    build: {
        lib: {
            entry: './lib/index.ts',
            name: 'Merge',
            fileName: 'index'
        }
    },
    plugins: [
        replace({
            values: {
                __author__: pkg.author,
                __buildDate__: `${d.getFullYear()}/${(d.getMonth() + 1).toString().padStart(2, '0')}/${d.getDate().toString().padStart(2, '0')} ${d.getHours().toString().padStart(2, '0')}:${d.getMinutes().toString().padStart(2, '0')}`,
                __buildVersion__: pkg.version,
            },
            preventAssignment: false,
        }),
        dts()
    ]
});