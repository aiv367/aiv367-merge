# aiv367-merge

#### 介绍
本模块提供两个对象深拷贝函数，一种是遇数组合并模式（merge），一种是遇数组替换模式 (merge2)。

#### Gitee
[https://gitee.com/aiv367/aiv367-merge](https://gitee.com/aiv367/aiv367-merge)

#### 安装
```
npm i aiv367-merge --save
```
#### 使用
ES6 Module:
```js
import merge from 'aiv367-merge'; //遇数组合并
import {merge2} from 'aiv367-merge'; //遇数组替换

merge({ a: [1, 2] }, { a: [3, 4] }, { c: 1 });
//返回结果
// {a: [1, 2, 3, 4], c: 1}

merge2({ a: [1, 2] }, { a: [3, 4] }, { c: 1 });
//返回结果
// {a: [3, 4], c: 1}
```