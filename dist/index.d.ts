/*!
 * 对象深拷贝模块
 * @author: __author__
 * @version: __buildVersion__
 * @buildDate：__buildDate__
 */
export declare function merge1(target: object, ...arg: any[]): object;
export declare function merge2(target: object, ...arg: any[]): object;
declare const merge: typeof merge1;
export default merge;
