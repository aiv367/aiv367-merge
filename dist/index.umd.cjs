(function(o,n){typeof exports=="object"&&typeof module<"u"?n(exports):typeof define=="function"&&define.amd?define(["exports"],n):(o=typeof globalThis<"u"?globalThis:o||self,n(o.Merge={}))})(this,function(o){"use strict";/*!
 * 对象深拷贝模块
 * @author: aiv367 <aiv367@qq.com>
 * @version: 1.1.2
 * @buildDate：2022/12/21 16:06
 */function n(f){return Object.prototype.toString.call(f)==="[object Object]"}function d(f,...u){return u.reduce((l,i)=>i==null?l:Object.keys(i).reduce((e,r)=>{const t=i[r];return n(t)&&n(e[r])?e[r]=d(e[r]?e[r]:{},t):Array.isArray(t)&&Array.isArray(e[r])?e[r]=e[r].concat(t):e[r]=t,e},l),f)}function p(f,...u){return u.reduce((l,i)=>i==null?l:Object.keys(i).reduce((e,r)=>{const t=i[r];return n(t)&&n(e[r])?e[r]=a(e[r]?e[r]:{},t):Array.isArray(t)&&Array.isArray(e[r])?e[r]=t.map((g,s)=>{if(n(g)){const j=e[r]?e[r]:[];return a(j[s]?j[s]:{},g)}else return g}):e[r]=t,e},l),f)}const a=d;o.default=a,o.merge1=d,o.merge2=p,Object.defineProperties(o,{__esModule:{value:!0},[Symbol.toStringTag]:{value:"Module"}})});
