/*!
 * 对象深拷贝模块
 * @author: aiv367 <aiv367@qq.com>
 * @version: 1.1.2
 * @buildDate：2022/12/21 16:06
 */
function o(i) {
  return Object.prototype.toString.call(i) === "[object Object]";
}
function g(i, ...a) {
  return a.reduce((l, n) => n == null ? l : Object.keys(n).reduce((r, t) => {
    const e = n[t];
    return o(e) && o(r[t]) ? r[t] = g(r[t] ? r[t] : {}, e) : Array.isArray(e) && Array.isArray(r[t]) ? r[t] = r[t].concat(e) : r[t] = e, r;
  }, l), i);
}
function p(i, ...a) {
  return a.reduce((l, n) => n == null ? l : Object.keys(n).reduce((r, t) => {
    const e = n[t];
    return o(e) && o(r[t]) ? r[t] = O(r[t] ? r[t] : {}, e) : Array.isArray(e) && Array.isArray(r[t]) ? r[t] = e.map((f, d) => {
      if (o(f)) {
        const j = r[t] ? r[t] : [];
        return O(
          j[d] ? j[d] : {},
          f
        );
      } else
        return f;
    }) : r[t] = e, r;
  }, l), i);
}
const O = g;
export {
  O as default,
  g as merge1,
  p as merge2
};
